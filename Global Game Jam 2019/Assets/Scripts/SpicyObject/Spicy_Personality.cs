﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Personality", menuName = "Spicey")]
public class Spicy_Personality : ScriptableObject
{
    public Sprite standing, walking, talking, dancing, sitting;

    public string nameOfSpicey;
    public Vector2 roomLocation;
    public Spicy_Personality bestFriend;
    public ConversationSpotManager.TalkingSubjects favouriteConversationTopic;
    public float sitTime;

}
