﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spicies_Controller : MonoBehaviour
{

    private SpriteRenderer _spriteRenderer;
    private GoToPoint _gotopointScript;
    public enum CurrentTask
    {
        DoingNothing,
        GoToTalkingPoint,
        GoSit,
        GoToRoomForFun,
        GoToRoomForMiniGames,
        Talking,
        WaitingToTalk,
        Sitting,


    }




    public Spicy_Personality personality;

    public CurrentTask currentTask;


    float TaskTimer = 0;
    float animTimer = 0;
    float TalkTimer = 0;

    // Start is called before the first frame update
    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();

        _spriteRenderer.sprite = personality.standing;
        _gotopointScript = GetComponent<GoToPoint>();

        transform.position = personality.roomLocation;

    }

    // Update is called once per frame
    void Update()
    {
        switch (currentTask)
        {

            case CurrentTask.DoingNothing:
                _spriteRenderer.sprite = personality.standing;
                CurrentTask toDo = ActionDecider();
                SetUpTaskInMotion(toDo);
                break;
            case CurrentTask.GoToRoomForFun:
                WalkCycle();
                if (_gotopointScript.MoveAlongPath(3f))
                {
                    currentTask = CurrentTask.DoingNothing;
                }
                break;
            case CurrentTask.GoSit:
                WalkCycle();
                if (_gotopointScript.MoveAlongPath(3f))
                {
                    currentTask = CurrentTask.Sitting;
                }
                break;
            case CurrentTask.Sitting:
                _spriteRenderer.sprite = personality.sitting;
                TaskTimer += Time.deltaTime;

                if (TaskTimer > personality.sitTime)
                {
                    ChairManager.instance.ClearChair(transform.position);
                    currentTask = CurrentTask.DoingNothing;
                }

                break;
            case CurrentTask.GoToTalkingPoint:
                WalkCycle();
                if (_gotopointScript.MoveAlongPath(3f))
                {
                    currentTask = CurrentTask.WaitingToTalk;
                }
                break;
            case CurrentTask.WaitingToTalk:
                _spriteRenderer.sprite = personality.standing;
                if (ConversationSpotManager.instance.ConversationCanStart(transform.position))
                {
                    currentTask = CurrentTask.Talking;
                }
                break;
            case CurrentTask.Talking:
                _spriteRenderer.sprite = personality.talking;
                if (ConversationSpotManager.instance.RightSide(transform.position))
                {
                    transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
                }
                else
                {
                    transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
                }
                TaskTimer += Time.deltaTime;

                if (TaskTimer > 8f)
                {
                    currentTask = CurrentTask.DoingNothing;
                    ConversationSpotManager.instance.ClearConversation(transform.position);
                    TalkTimer = 0;
                }

                TalkTimer += Time.deltaTime;

                if (TalkTimer > 2f)
                {
                    GameObject bubble = new GameObject("bubble");
                    bubble.transform.position = transform.position + Vector3.up * 0.8f;
                    SpriteRenderer bubbleRenderer = bubble.AddComponent<SpriteRenderer>();
                    bubbleRenderer.sortingLayerName = "Rooms";
                    bubbleRenderer.sortingOrder = 10;
                    int topic = (int)Random.Range(0f, 12.999999f);

                    int r = Random.Range(-1, 2);
                    if (r < 0)
                    {
                        topic = (int)personality.favouriteConversationTopic;
                    }

                    bubbleRenderer.sprite = ConversationSpotManager.instance.talkingImages[topic];
                    Destroy(bubble, 1.8f);
                    TalkTimer = 0;

                }



                break;
        }

    }

    private void WalkCycle()
    {
        animTimer += Time.deltaTime * 3f;

        if ((int)(animTimer) % 2 > 0)
        {
            _spriteRenderer.sprite = personality.standing;
        }
        else
        {
            _spriteRenderer.sprite = personality.walking;
        }

    }


    private void SetUpTaskInMotion(CurrentTask toDo)
    {
        TaskTimer = 0;
        switch (toDo)
        {
            case CurrentTask.GoSit:

                Vector2 chairPos = ChairManager.instance.RequestChair();

                if (chairPos != Vector2.zero)
                {
                    _gotopointScript.CreatePath(chairPos.x, (int)chairPos.y);
                    currentTask = CurrentTask.GoSit;
                }
                else
                {
                    currentTask = CurrentTask.DoingNothing;

                }
                break;
            case CurrentTask.GoToRoomForFun:
                _gotopointScript.CreatePath(personality.roomLocation.x, (int)personality.roomLocation.y);
                currentTask = CurrentTask.GoToRoomForFun;
                break;

            case CurrentTask.GoToRoomForMiniGames:
                transform.position = new Vector2(personality.roomLocation.x, (int)personality.roomLocation.y);
                currentTask = CurrentTask.GoToRoomForMiniGames;
                break;

            case CurrentTask.GoToTalkingPoint:
                Vector2 convoPos = ConversationSpotManager.instance.RequestConversationSpot();
                if (convoPos != Vector2.zero)
                {
                    _gotopointScript.CreatePath(convoPos.x, (int)convoPos.y);
                    currentTask = CurrentTask.GoToTalkingPoint;
                }
                else
                {
                    currentTask = CurrentTask.DoingNothing;
                }
                break;

        }
    }




    private CurrentTask ActionDecider()
    {
        int r = (int)Random.Range(1f, 3.999999999f);
        return (CurrentTask)r;
    }


}
