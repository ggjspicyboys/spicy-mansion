﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToPoint : MonoBehaviour
{
    List<Vector2> path = new List<Vector2>();
    private int currentPoint;

    // Start is called before the first frame update
    void Start()
    {

    }


    public void ResetStuff()
    {
        currentPoint = 0;
        path = null;
    }

    public bool MoveAlongPath(float speed)
    {
        if ((Vector2)transform.position == path[currentPoint])
        {
            if (currentPoint < path.Count - 1)
            {
                currentPoint++;
            }
        }
        else
        {
            MoveToPoint(path[currentPoint].x, (int)path[currentPoint].y, speed);
        }


        if ((Vector2)transform.position == path[path.Count - 1])
        {
            transform.position = path[path.Count - 1];
            ResetStuff();
            return true;
        }
        return false;
    }


    //moves to each point along the path
    private void MoveToPoint(float x, int y, float speed)
    {
        if (transform.position.x > x)
        {
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
        }
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(x, y), Time.deltaTime * speed);
    }



    //enerates all the waypoints needs to reach the destination
    public void CreatePath(float x, int y)
    {
        List<Vector2> path = new List<Vector2>();

        if (transform.position.y != y)
        {
            path.Add(new Vector2(1.85f, transform.position.y));
            path.Add(new Vector2(1.85f, y));

        }
        path.Add(new Vector2(x, y));

        this.path = path;
    }
}
