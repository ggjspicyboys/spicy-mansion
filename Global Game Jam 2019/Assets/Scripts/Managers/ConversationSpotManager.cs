﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationSpotManager : MonoBehaviour
{
    public static ConversationSpotManager instance = null;

    public float TalkingDistance;

    public List<ConversationPosition> convoPositions = new List<ConversationPosition>();

    public enum TalkingSubjects
    {
        Smiley,
        Pencil,
        Music,
        Fire,
        Weather,
        MixrLogo,
        TwitchLogo,
        VideoGames,
        Pidgeon,
        RedBow,
        Pizza,
        Heart,
        Money
    }

    public List<Sprite> talkingImages = new List<Sprite>();

    [System.Serializable]
    public class ConversationPosition
    {
        public Vector2 Location;
        public int Occupants;
        public bool rightOccupant, LeftOccupant;
    }

    void Start()
    {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        convoPositions = ShuffleList(convoPositions);

    }


    public Vector2 RequestConversationSpot()
    {
        foreach (ConversationPosition p in convoPositions)
        {
            if (p.Occupants < 2)
            {
                if (p.Occupants == 0)
                {
                    p.Occupants++;
                    return new Vector2(p.Location.x - TalkingDistance, p.Location.y);
                }
                else
                {
                    p.Occupants++;
                    return new Vector2(p.Location.x + TalkingDistance, p.Location.y);
                }
            }
        }

        return Vector2.zero;
    }

    public bool ConversationCanStart(Vector2 position)
    {
        foreach (ConversationPosition p in convoPositions)
        {
            if (position + Vector2.right * TalkingDistance == p.Location)
            {
                p.LeftOccupant = true;
            }
            if (position - Vector2.right * TalkingDistance == p.Location)
            {
                p.rightOccupant = true;
            }
            if (p.rightOccupant && p.LeftOccupant)
            {
                return true;
            }
        }

        return false;
    }

    public void ClearConversation(Vector2 position)
    {
        foreach (ConversationPosition p in convoPositions)
        {
            if (p.Location + Vector2.right * TalkingDistance == position || p.Location - Vector2.right * TalkingDistance == position)
            {
                p.rightOccupant = false;
                p.LeftOccupant = false;
                p.Occupants = 0;
            }
        }
    }

    public bool RightSide(Vector2 position)
    {
        foreach (ConversationPosition p in convoPositions)
        {
            if (position + Vector2.right * TalkingDistance == p.Location)
            {
                return false;
            }
            if (position - Vector2.right * TalkingDistance == p.Location)
            {
                return true;
            }

        }

        return false;
    }


    private List<ConversationPosition> ShuffleList(List<ConversationPosition> list)
    {
        int listLength = list.Count;
        List<ConversationPosition> temp = new List<ConversationPosition>();
        List<int> n = new List<int>();


        while (temp.Count < listLength)
        {
            int r = (int)Random.Range(0, listLength - 0.00001f);

            if (!n.Contains(r))
            {
                temp.Add(list[r]);
                n.Add(r);

            }

        }


        return temp;


    }


}
