﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChairManager : MonoBehaviour
{

    public static ChairManager instance = null;

    [System.Serializable]
    public class ChairPosition
    {
        public Vector2 Location;
        public bool Occupied;

        public void SetOccupied(bool occupied)
        {
            this.Occupied = occupied;
        }
    }

    public List<ChairPosition> chairPositions = new List<ChairPosition>();

    // Start is called before the first frame update
    void Start()
    {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        chairPositions = ShuffleList(chairPositions);
    }

    public Vector2 RequestChair()
    {
        foreach (ChairPosition p in chairPositions)
        {
            if (!p.Occupied)
            {
                p.SetOccupied(true);
                return p.Location;
            }
        }
        return Vector2.zero;
    }

    public void ClearChair(Vector2 chair)
    {
        foreach (ChairPosition p in chairPositions)
        {
            if (chair == p.Location)
            {
                p.SetOccupied(false);
            }
        }
    }


    private List<ChairPosition> ShuffleList(List<ChairPosition> list)
    {
        int listLength = list.Count;
        List<ChairPosition> temp = new List<ChairPosition>();
        List<int> n = new List<int>();


        while (temp.Count < listLength)
        {
            int r = (int)Random.Range(0, listLength - 0.00001f);

            if (!n.Contains(r))
            {
                temp.Add(list[r]);
                n.Add(r);

            }

        }


        return temp;


    }

}
