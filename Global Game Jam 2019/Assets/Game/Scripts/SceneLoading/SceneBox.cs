﻿using UnityEngine;
using System.Collections;

public class SceneBox : MonoBehaviour
{
	public HotelCamera hotelCamera;
	public int sceneIndex;

	private void OnMouseDown()
	{
		if (!hotelCamera.zoomed && !SceneController.instance.paused)
		{
			StartCoroutine(hotelCamera.Zoom(transform.position, hotelCamera.zoomSize,true));
			SceneController.instance.StartLoadGameScene(sceneIndex, hotelCamera);
		}
	}

    private void OnMouseEnter()
    {
        gameObject.ShakeScale(new Vector3(0.005f, 0.005f, 0f), 0.25f, 0f);
    }
}
