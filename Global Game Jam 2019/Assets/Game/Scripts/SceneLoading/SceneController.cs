﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
	public static SceneController instance = null;
	public const int MainMenuIndex = 0;
	public const int GameMainIndex = 1;
	public const int HotelIndex = 2;

	public int loadedIndex = 0;
	public bool paused = false;

	public HotelCamera hotelCamera;

	private void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);

		DontDestroyOnLoad(gameObject);
	}

	public void StartLoadGameScene(int sceneIndex, HotelCamera camera, LoadSceneMode mode = LoadSceneMode.Additive)
	{
		if (loadedIndex != sceneIndex)
			StartCoroutine(LoadGameScene(sceneIndex, camera, mode));
	}

	public void StartLoadScene(int sceneIndex, LoadSceneMode mode = LoadSceneMode.Additive)
	{
		if (loadedIndex != sceneIndex)
			StartCoroutine(LoadScene(sceneIndex, mode));
	}

	public IEnumerator Unload(int loadIndex)
	{
		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(loadIndex));
		yield return SceneManager.UnloadSceneAsync(loadIndex);
	}

	public IEnumerator LoadScene(int sceneIndex, LoadSceneMode mode)
	{
		enabled = false;
		if (loadedIndex > 0 && mode != LoadSceneMode.Single)
			yield return SceneManager.UnloadSceneAsync(loadedIndex);

		loadedIndex = sceneIndex;

		yield return SceneManager.LoadSceneAsync(sceneIndex, mode);

		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneIndex));
		enabled = true;
	}
	public IEnumerator LoadGameScene(int sceneIndex, HotelCamera camera, LoadSceneMode mode)
	{
		enabled = false;

		if (loadedIndex > 0 && mode != LoadSceneMode.Single)
			yield return SceneManager.UnloadSceneAsync(loadedIndex);

		loadedIndex = sceneIndex;

		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneIndex, mode);
		asyncLoad.allowSceneActivation = false;

		while (!asyncLoad.isDone)
		{
			if (camera.zoomed && asyncLoad.progress >= 0.9f)
			{
				asyncLoad.allowSceneActivation = true;
			}
			yield return null;
		}

		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneIndex));
		enabled = true;
	}

}
