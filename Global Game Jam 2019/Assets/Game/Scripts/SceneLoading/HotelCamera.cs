﻿using System.Collections;
using UnityEngine;

public class HotelCamera : MonoBehaviour
{
	public Camera mainCamera;
	Vector2 startPos;
	float startSize;

	public float zoomSize;
	public bool zoomed = false;

	private void Awake()
	{
		mainCamera = GetComponent<Camera>();
		startPos = transform.position;
		startSize = mainCamera.orthographicSize;
	}

	private void Start()
	{
		SceneController.instance.hotelCamera = this;
	}

	public void ZoomOut()
	{
		HandleCursor.HandleCursorVisibility(true);

		StartCoroutine(Zoom(startPos,startSize,false));
	}

	public IEnumerator Zoom(Vector2 zoomTo, float zoomSize, bool zoomIn)
	{
		zoomed = !zoomIn;

		float t = Time.time;
		float d = Vector2.Distance(transform.position, zoomTo);
		while (Vector2.Distance(transform.position, zoomTo) > .001f && Mathf.Abs(mainCamera.orthographicSize - zoomSize) > 0.01f)
		{
			float distCovered = (Time.time - t) * .5f;
			float fracJourney = distCovered / d;

			transform.position = Vector2.Lerp(transform.position, zoomTo, fracJourney);
			transform.position = new Vector3(transform.position.x, transform.position.y,-10);
			mainCamera.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, zoomSize, fracJourney);

			yield return null;
		}

		t = Time.time;
		while (Time.time - t < .5f)
		{
			yield return null;
		}

		zoomed = zoomIn;
	}
}
