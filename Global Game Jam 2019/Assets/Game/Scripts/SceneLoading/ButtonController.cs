﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
	public void StartLoadScene(int sceneIndex, LoadSceneMode mode)
	{
		SceneController.instance.StartLoadScene(sceneIndex,mode);
	}

	public void Resume()
	{
		Resume(SceneController.GameMainIndex);
	}

	public void Resume(int closeIndex)
	{
		StartCoroutine(SceneController.instance.Unload(closeIndex));
		SceneController.instance.loadedIndex = SceneController.HotelIndex;
		SceneController.instance.paused = false;
	}

	public void ZoomOut()
	{
		SceneController.instance.hotelCamera.ZoomOut();
	}

	public void LoadHotelScene()
	{
		StartLoadScene(SceneController.HotelIndex, LoadSceneMode.Single);
	}

	public void LoadMainMenu()
	{
		StartLoadScene(SceneController.MainMenuIndex, LoadSceneMode.Single);
	}

	public void LoadGameMenu()
	{
		StartLoadScene(SceneController.GameMainIndex,LoadSceneMode.Additive);
		SceneController.instance.paused = true;
	}

	public void ExitGame()
	{
		Application.Quit();
	}
}
