﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour
{
    public float countdownTime;
    private float maxTime;

    public Image timer;

    private void Start()
    {
        maxTime = countdownTime;
    }

    private void Update()
    {
        countdownTime -= Time.deltaTime;

        timer.fillAmount = Mathf.Lerp(timer.fillAmount, countdownTime / maxTime, 0.1f);
    }
}
