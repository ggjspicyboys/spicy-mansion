﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bubble : MonoBehaviour
{
    public float increase;
    public float maxSize;

    public Text scoreDisplay, highscoreDisplay;

    private UITimer timer;

	public ButtonController buttonController;

    private int score;

    private void Awake()
    {
        timer = FindObjectOfType<UITimer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetScale();
    }

    // Update is called once per frame
    void Update()
    {
        Grow();
        SetDisplays();
        CheckTime();
    }

    private void FixedUpdate()
    {
        Shrink();
    }

    private void Grow()
    {
        if (Input.GetMouseButtonDown(0))
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Dan/Dan_BubbleGrow").start();
            transform.localScale += new Vector3(increase, increase);
            Camera.main.gameObject.ShakePosition(new Vector3(0.05f, 0.05f, 0f), 0.1f, 0f);
            CheckScale();
        }
    }

    private void Shrink()
    {
        if (transform.localScale.x > 0f)
        {
            transform.localScale -= new Vector3(increase / 25f, increase / 25f, 0f);
        }
    }

    private void CheckScale()
    {
        if(transform.localScale.x >= maxSize)
        {
            ResetScale();
            score++;
            CheckHighscore();
            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Dan/Dan_BubblePop").start();
        }
    }

    private void CheckHighscore()
    {
        if(score > PlayerPrefs.GetInt("danHighscore"))
        {
            PlayerPrefs.SetInt("danHighscore", score);
        }
    }

    private void ResetScale()
    {
        transform.localScale = new Vector3(0f, 0f, 1f);
    }

    private void SetDisplays()
    {
        scoreDisplay.text = score.ToString();
        highscoreDisplay.text = PlayerPrefs.GetInt("danHighscore").ToString();
    }

    private void CheckTime()
    {
        if(timer.countdownTime <= 0f)
        {
			buttonController.ZoomOut();
			buttonController.Resume(3);
        }
    }
}
