﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdSpawner : MonoBehaviour
{
    public float minTime, maxTime;

    public GameObject birdPrefab;

    public bool over;

	public	ButtonController buttonController;
	public int spawnIndex;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnBird());
    }

    private IEnumerator SpawnBird()
    {
        while (!over)
        {
            GameObject instBird = Instantiate(birdPrefab);

            instBird.transform.position = new Vector3(instBird.transform.position.x, instBird.transform.position.y, -.01f);

            yield return new WaitForSeconds(Random.Range(minTime, maxTime));
        }

        foreach(Bird bird in FindObjectsOfType<Bird>())
        {
            Destroy(bird.gameObject);
        }
    }

	public void GameOver()
	{
		over = true;
		if (buttonController != null)
		{
			buttonController.ZoomOut();
			buttonController.Resume(spawnIndex);
		}
	}
}