﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour {

    public bool canMove = true;

	public Camera myCamera;

    private void Start()
    {
        HandleCursor.HandleCursorVisibility(false);
		GetMousePositionInWorldSpace.currentCamera = myCamera;

	}

    // Update is called once per frame
    void Update () {

        if (canMove)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(GetMousePositionInWorldSpace.GetMousePosition().x, transform.position.y, transform.position.z), 0.25f);
        }
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -8f, 8f), transform.position.y, transform.position.z);

    }
}
