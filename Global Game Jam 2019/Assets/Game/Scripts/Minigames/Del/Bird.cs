﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    public float maxX, spawnY;

    // Start is called before the first frame update
    void Start()
    {
        SpawnRandomX();
    }

    private void SpawnRandomX()
    {
        transform.position = new Vector3(Random.Range(-maxX, maxX), spawnY, transform.position.z);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<DelPlayer>().score++;
            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Del/Del_Birds").start();
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag == "Ground")
		{
			//TODO: END GAME
			FindObjectOfType<BirdSpawner>().GameOver();
            Destroy(this.gameObject);
        }
    }
}
