﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DelPlayer : MonoBehaviour
{
    public int score;

    public Text scoreDisplay, highscoreDisplay;

    // Update is called once per frame
    void Update()
    {
        CheckHighscore();
        SetDisplays();
    }

    private void CheckHighscore()
    {
        if (score > PlayerPrefs.GetInt("delHighscore"))
        {
            PlayerPrefs.SetInt("delHighscore", score);
        }
    }

    private void SetDisplays()
    {
        scoreDisplay.text = score.ToString();
        highscoreDisplay.text = PlayerPrefs.GetInt("delHighscore").ToString();
    }
}
