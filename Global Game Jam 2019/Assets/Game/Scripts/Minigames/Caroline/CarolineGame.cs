﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarolineGame : MonoBehaviour
{
    private int score = 0;
	public UITimer timer;

	public ButtonController bc;
	public int sceneIndex;
    public Text scoreDisplay, highscoreDisplay;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Caroline/Caro_Slurp").start();
            score++;
        }

        CheckHighscore();
        SetDisplays();
		if (timer.countdownTime <= 0f)
		{
			bc.ZoomOut();
			bc.Resume(sceneIndex);
		}

    }

    private void CheckHighscore()
    {
        if (score > PlayerPrefs.GetInt("carolineHighscore"))
        {
            PlayerPrefs.SetInt("carolineHighscore", score);
        }
    }

    private void SetDisplays()
    {
        scoreDisplay.text = score.ToString();
        highscoreDisplay.text = PlayerPrefs.GetInt("carolineHighscore").ToString();
    }


}
