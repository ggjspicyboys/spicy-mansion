﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JustinGame : MonoBehaviour
{
    public float minInterval, maxInterval;

    public float threshold;

    private int score;

    public Text scoreDisplay, highscoreDisplay;

    private bool hit = false, canHit = false;
    private bool over = false;

    public Sprite down, up;

    private SpriteRenderer sr;

	public SpriteRenderer pow;

	public ButtonController buttonController;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Gameplay());
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
        CheckHighscore();
        SetDisplays();
    }

    private void HandleInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (canHit)
            {
                hit = true;
                FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Justin/Just_Broom").start();
                sr.sprite = up;
            }
        }
    }

    private IEnumerator Gameplay()
    {
        while (!over)
        {
            yield return new WaitForSeconds(Random.Range(minInterval, maxInterval));

            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Justin/Just_Speakers").start();

            canHit = true;
			pow.enabled = true;
            Camera.main.gameObject.ShakePosition(new Vector3(0.25f, 0.25f, 0.25f), 0.25f, 0f);
            yield return new WaitForSeconds(threshold);
            canHit = false;
			pow.enabled = false;

			if (!hit)
            {
                over = true;
				buttonController.ZoomOut();
				buttonController.Resume(8);
            }
            else
            {
                score++;
                hit = false;
            }

            yield return new WaitForSeconds(0.1f);

            sr.sprite = down;
        }
    }


    private void CheckHighscore()
    {
        if (score > PlayerPrefs.GetInt("justinHighscore"))
        {
            PlayerPrefs.SetInt("justinHighscore", score);
        }
    }

    private void SetDisplays()
    {
        scoreDisplay.text = score.ToString();
        highscoreDisplay.text = PlayerPrefs.GetInt("justinHighscore").ToString();
    }
}
