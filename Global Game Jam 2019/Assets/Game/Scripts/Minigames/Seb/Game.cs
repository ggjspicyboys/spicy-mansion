﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

    public Camera cam;
	public SpriteRenderer background;

    private int mouseButton1, mouseButton2;

    private bool over = false;


	public ButtonController buttonController;
	public int sceneIndex;

	// Use this for initialization
	void Start () {
        GetRandomKeys();

        InvokeRepeating("GetRandomKeys", 3f, 3f);
	}
	
	// Update is called once per frame
	void Update () {
        if (!over)
        {
            Suffer();
            Move();
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
		if (collision.gameObject.tag == "Spicy")
		{
			Leave();
		}
    }

    private void Leave()
    {
        PlayerPrefs.SetInt("sebHighscore", PlayerPrefs.GetInt("sebHighscore") + 1);
		if (buttonController != null)
		{
			buttonController.ZoomOut();
			buttonController.Resume(sceneIndex);
		}
    }

    private void Move()
    {
        if (Input.GetMouseButton(mouseButton1))
        {
            transform.position += new Vector3(-0.05f, 0.0f, 0f);
        }
        if (Input.GetMouseButton(mouseButton2))
        {
            transform.position += new Vector3(0.05f, 0.0f, 0f);
        }
    }

    private void GetRandomKeys()
    {
        mouseButton1 = Random.Range(0, 2);

        mouseButton2 = Random.Range(0, 2);

        while(mouseButton2 == mouseButton1)
        {
            mouseButton2 = Random.Range(0, 2);
        }
    }

    private void Suffer()
    {
        cam.gameObject.ShakePosition(new Vector3(0.75f, 0.75f, 0.75f), 0.1f, 0f);
        //background.color = Random.ColorHSV();
    }
}
