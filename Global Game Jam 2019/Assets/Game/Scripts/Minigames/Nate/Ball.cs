﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    public Text scoreDisplay, highscoreDisplay;

    private int score;
    private Rigidbody2D rb;

	public ButtonController buttonController;
	public int sceneIndex;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckHighscore();
        SetDisplays();
    }

    private void OnMouseDown()
    {
        score++;
        FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Nate/Nate_Donkey").start();
        rb.AddForce(new Vector3(Random.Range(-10f, 10f), 10f, 0f), ForceMode2D.Impulse);
        rb.AddTorque(Random.Range(-1f, 1f), ForceMode2D.Impulse);
    }

    private void CheckHighscore()
    {
        if (score > PlayerPrefs.GetInt("nateHighscore"))
        {
            PlayerPrefs.SetInt("nateHighscore", score);
        }
    }

    private void SetDisplays()
    {
        scoreDisplay.text = score.ToString();
        highscoreDisplay.text = PlayerPrefs.GetInt("nateHighscore").ToString();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Nate/Nate_Fart").start();
            Destroy(this.gameObject);
			buttonController.ZoomOut();
			buttonController.Resume(sceneIndex);
		}
    }
}
