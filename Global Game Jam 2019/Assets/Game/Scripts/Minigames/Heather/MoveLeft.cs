﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour {

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start () {
        transform.position = new Vector3(15f, Random.Range(-5f, 5f), transform.position.z);
        rb.velocity = new Vector3(Random.Range(-15, -7), 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.x < -20f)
        {
            Destroy(this.gameObject);
        }
	}
}
