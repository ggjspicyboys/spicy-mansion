﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowMouseY : MonoBehaviour
{
    public bool canMove = true;

    private int score = 0;
    private bool over = false;

	public int spawnIndex;

	public Camera myCamera;

	public ButtonController buttonController;

    public Text scoreDisplay, highscoreDisplay;

    private void Start()
    {
		GetMousePositionInWorldSpace.currentCamera = myCamera;
        HandleCursor.HandleCursorVisibility(false);
        StartCoroutine(GetPoints());
    }

    // Update is called once per frame
    void Update()
    {

        if (canMove)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, GetMousePositionInWorldSpace.GetMousePosition().y, transform.position.z), 0.25f);
        }
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -4.5f, 4.5f), transform.position.z);

        CheckHighscore();
        SetDisplays();
		if (over)
		{
			buttonController.ZoomOut();
			buttonController.Resume(spawnIndex);
		}
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Dragon")
        {
            //TODO: LOSE
            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Heather/Hea_Roar").start();
            over = true;
        }
    }

    private IEnumerator GetPoints()
    {
        while (!over)
        {
            yield return new WaitForSeconds(1f);
            score++;
        }
    }

    private void CheckHighscore()
    {
        if (score > PlayerPrefs.GetInt("heatherHighscore"))
        {
            PlayerPrefs.SetInt("heatherHighscore", score);
        }
    }

    private void SetDisplays()
    {
        scoreDisplay.text = score.ToString();
        highscoreDisplay.text = PlayerPrefs.GetInt("heatherHighscore").ToString();
    }
}
