﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour {

    public GameObject spaghettiSplatter;

    public int score;

    public Text scoreDisplay, highscoreDisplay;

	public Camera myCamera;

	private void Start()
	{
		GetMousePositionInWorldSpace.currentCamera = myCamera;
	}

	// Update is called once per frame
	void Update () {
        transform.position = GetMousePositionInWorldSpace.GetMousePosition() + new Vector3(0f, 0f, 1);

        if (Input.GetMouseButtonDown(0))
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Mike/Mike_Squish").start();
            FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Mike/Mike_Spaghetti").start();
            this.gameObject.ShakeScale(new Vector3(0.01f, 0.01f, 0.01f), 0.25f, 0f);
            Camera.main.gameObject.ShakePosition(new Vector3(0.25f, 0.25f, 0.25f), 0.2f, 0f);

            GameObject instSplat = Instantiate(spaghettiSplatter);
            instSplat.transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
        }

        CheckHighscore();
        SetDisplays();
	}

    private void CheckHighscore()
    {
        if (score > PlayerPrefs.GetInt("mikeHighscore"))
        {
            PlayerPrefs.SetInt("mikeHighscore", score);
        }
    }

    private void SetDisplays()
    {
        scoreDisplay.text = score.ToString();
        highscoreDisplay.text = PlayerPrefs.GetInt("mikeHighscore").ToString();
    }
}
