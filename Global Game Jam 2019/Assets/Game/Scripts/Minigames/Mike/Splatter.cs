﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splatter : MonoBehaviour
{

    private SpriteRenderer sr;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, Mathf.Lerp(sr.color.a, 0f, 0.05f));
    }

    private void Update()
    {
        if(sr.color.a <= 0.05f)
        {
            Destroy(this.gameObject);
        }
    }
}
