﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cockroach : MonoBehaviour
{

    public float minSpeed, maxSpeed;

    public float maxY;

    public float spawnX;

    private Rigidbody2D rb;

    private float speed;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        speed = Random.Range(minSpeed, maxSpeed);
        transform.position = new Vector3(spawnX, Random.Range(-maxY, maxY), 1f);
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        rb.velocity = new Vector3(speed, 0f, 0f);
    }

    private void OnMouseDown()
    {
        FindObjectOfType<Crosshair>().score++;
        FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Mike/Mike_Roaches").start();

        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Spaghetti")
        {
			FindObjectOfType<BirdSpawner>().GameOver();

			Destroy(this.gameObject);
        }
    }
}
