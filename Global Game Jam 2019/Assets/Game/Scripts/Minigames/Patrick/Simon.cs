﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Simon : MonoBehaviour
{
    public Text scoreDisplay, highscoreDisplay;

    public GameObject[] buttons;

    private bool over = false;

    private int score = 0;

    private List<int> selections = new List<int>();

    public bool chosen;
    public int chosenNumber;

    public bool canChoose = false;

	public ButtonController buttonController;
	public int sceneIndex = 11;

    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject button in buttons)
        {
            button.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
        }

        StartCoroutine(HandleGame());
    }

    private void Update()
    {
        CheckHighscore();
        SetDisplays();
    }

    private IEnumerator HandleGame()
    {
        while (!over)
        {
            selections.Add((int)Random.Range(0, 4));

            for(int i = 0; i < selections.Count; i++)
            {
                buttons[selections[i]].GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
                FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Pat/Pat_BWA" + (selections[i] + 1).ToString()).start();
                Camera.main.gameObject.ShakePosition(new Vector3(0.1f, 0.1f, 0f), 0.25f, 0f);
                yield return new WaitForSeconds(0.5f);

                buttons[selections[i]].GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);

                yield return new WaitForSeconds(0.25f);
            }

            for (int i = 0; i < selections.Count; i++)
            {
                canChoose = true;
                while (!chosen)
                {
                    yield return null;
                }
                canChoose = false;

                if (chosenNumber != selections[i])
                {
                    over = true;
                    break;
                }
                else
                {
                    buttons[selections[i]].GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
                    FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Pat/Pat_Simon" + (selections[i] + 1).ToString()).start();
                    yield return new WaitForSeconds(0.5f);

                    buttons[selections[i]].GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);

                    chosen = false;
                }
            }

            if (!over)
            {
                score++;
				yield return new WaitForSeconds(1);
            }
        }
		buttonController.ZoomOut();
		buttonController.Resume(sceneIndex);
    }

    private void CheckHighscore()
    {
        if (score > PlayerPrefs.GetInt("patrickHighscore"))
        {
            PlayerPrefs.SetInt("patrickHighscore", score);
        }
    }

    private void SetDisplays()
    {
        scoreDisplay.text = score.ToString();
        highscoreDisplay.text = PlayerPrefs.GetInt("patrickHighscore").ToString();
    }
}
