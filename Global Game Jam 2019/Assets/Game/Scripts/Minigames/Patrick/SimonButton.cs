﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonButton : MonoBehaviour
{

    public int number;

    private Simon simon;

    private void Awake()
    {
        simon = FindObjectOfType<Simon>();
    }

    private void OnMouseDown()
    {
        if (simon.canChoose)
        {
            simon.chosenNumber = number;
            simon.chosen = true;
        }
    }
}
