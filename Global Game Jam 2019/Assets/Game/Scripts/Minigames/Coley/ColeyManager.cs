﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColeyManager : MonoBehaviour
{
	public List<Coleysfoodmouthers> mouthers;

	public GameObject foodPrefab;
	public GameObject rockPrefab;

	public ButtonController buttonController;

	int score;

	public GameObject eatPos;

	public float speed;
	public float spawnTime;

	public Text currentScore;
	public Text highScore;

	public Transform startPos;

	private UITimer timer;
	bool over;

	public SpriteRenderer coley;
	public Sprite[] sprites;


	private void Awake()
	{
		timer = FindObjectOfType<UITimer>();
	}

	private void Start()
	{
		SpawnFoodMouthers();
		highScore.text = PlayerPrefs.GetInt("coleysScore")+"";
	}

	private void Update()
	{
		
		spawnTime -= Time.deltaTime;

		if (!over)
		{
			if (spawnTime <= 0)
			{
				SpawnFoodMouthers();
				spawnTime = Random.Range(1f, 3f);
				speed = Random.Range(1f + score, 3f + score);
			}

			foreach (Coleysfoodmouthers mouther in mouthers)
			{
				mouther.transform.position += Vector3.right * speed * Time.deltaTime;
			}

			if (Input.GetMouseButtonDown(0))
			{
				EatClosestFood();
				coley.sprite = sprites[1];
			}
			if (Input.GetMouseButtonUp(0))
			{
				coley.sprite = sprites[0];
			}
		}

	}

	void EatClosestFood()
	{
		if (mouthers.Count > 0)
		{
			Collider[] colliders = new Collider[5];
			int amount = Physics.OverlapBoxNonAlloc(eatPos.transform.position, new Vector3(.5f, .5f, 10.5f), colliders);

			if (amount > 0 )
			{
				Coleysfoodmouthers mouther = colliders[0].gameObject.GetComponent<Coleysfoodmouthers>();
				if (mouther != null && mouther.GoodStuffz)
				{
                    FMODUnity.RuntimeManager.CreateInstance("event:/SFX/MINI GAMES/Coley/Col_Chomp").start();
					mouthers.Remove(mouther);
					Destroy(mouther.gameObject);
					score++;
					currentScore.text = score+"";
				}
				else
				{
					GameOver();
				}
			}
		}
	}

	void ChangeScore(int change)
	{
		score += change;
	}

	void SpawnFoodMouthers()
	{
		if (Random.Range(0f, 1f) > 0.2f)
		{
			GameObject newMouther = Instantiate(foodPrefab, startPos.position, Quaternion.identity);
			mouthers.Add(newMouther.GetComponent<Coleysfoodmouthers>());
		}
		else
		{
			GameObject newMouther = Instantiate(rockPrefab, startPos.position, Quaternion.identity);
			newMouther.transform.Rotate(new Vector3(0, 0, -90));
			mouthers.Add(newMouther.GetComponent<Coleysfoodmouthers>());

		}
	}

	bool CheckForGameOver()
	{
		return true;
	}

	void GameOver()
	{
		over = true;
		foreach (Coleysfoodmouthers mouther in mouthers)
		{
			Destroy(mouther.gameObject);
		}
		PlayerPrefs.SetInt("coleysScore", score);
		buttonController.ZoomOut();
		buttonController.Resume(5);
	}
}
