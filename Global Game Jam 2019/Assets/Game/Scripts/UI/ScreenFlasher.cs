﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class ScreenFlasher : MonoBehaviour {

    public int numberOfFrames;

    private CanvasGroup flashGroup;

    private void Awake()
    {
        flashGroup = GetComponent<CanvasGroup>();
    }

    public void StartFlash()
    {
        StartCoroutine(FlashScreen());
    }

    private IEnumerator FlashScreen()
    {
        flashGroup.alpha = 0.25f;

        for (int i = 0; i < numberOfFrames; i++)
        {
            yield return new WaitForEndOfFrame();
        }

        flashGroup.alpha = 0;
    }
}
